#!/bin/bash

# This script verifies the state of all disks in a ganeti cluster.
# Currently, it doesn't print the output of the command being run,
# however, this might be needed in the future.
# For now, this is enough.

set -u
set -o pipefail
set -E

MONITORING_OK=0
MONITORING_CRIT=2
MONITORING_UNKN=3

if [ ! -x /usr/sbin/gnt-cluster ]; then
  echo "UNKNOWN: /usr/sbin/gnt-cluster not found or not executable"
  exit $MONITORING_UNKN
fi

# Run only in ganeti master node, exit early otherwise
if [ "$(/usr/sbin/gnt-cluster getmaster)" != "$(hostname -f)" ]; then
  echo "OK: Not the master node"
  exit $MONITORING_OK
fi

GANETI_CLUSTER_VERIFY=$(/usr/sbin/gnt-cluster verify-disks >/dev/null 2>&1; echo $?)

if [ "$GANETI_CLUSTER_VERIFY" -eq 0 ]; then
  echo "cluster verify disks: success"
  exit $MONITORING_OK
else
  echo "cluster verify disks: error"
  exit $MONITORING_CRIT
fi
